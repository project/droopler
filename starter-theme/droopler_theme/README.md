# Compiling styles

Run the following command to compile Sass and watch for changes:
`ddev theme watch`

To compile the development version, run:
`ddev theme dev`

To compile the production, minified version, run:
`ddev theme`
