<?php

/**
 * @file
 * Theme social media helper functions.
 */

declare(strict_types=1);

/**
 * Get social media names.
 *
 * @return array
 *   Social media names.
 */
function _get_social_media_names(): array {
  return [
    'facebook',
    'twitter',
    'youtube',
    'instagram',
    'linkedin',
    'dribbble',
  ];
}
